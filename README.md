# Splunk sandbox


Trial account:

```
https://prd-p-7phd6.splunkcloud.com/
```

Tried "Universal Forwarder" but really maybe we're using "HTTP Event Collector".

To find my data, search:

```
index="history" source="http:fpotter-test-http-token"
```